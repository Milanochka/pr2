FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY /task1.java /app/task1.java
RUN javac /app/task1.java
ENTRYPOINT ["java", "-cp", "/app", "task1"]
